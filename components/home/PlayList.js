import styles from "./PlayList.module.scss";
import Icon from "react-icons-kit";
import { share2 } from "react-icons-kit/icomoon/share2";
// import Head from "next/head";
// import Link from "next/link";

const UserList = ({ profile, name, position }) => {
  return (
    <div className={styles.listItem}>
      <div className={styles.profile}>
        <img src={profile} alt="img" /*width="40" height="40"*/ />
      </div>
      <div>
        <p>{name}</p>
        <p>{position}</p>
      </div>
    </div>
  );
};

function PlayList() {
  const usersData = [
    {
      id: 1,
      profile:
        "https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
      name: "Ravindra B Narasappa",
      position: "Uber, Ex Amazon",
    },
    {
      id: 2,
      profile:
        "https://wedesignthemes.com/html/role/images/post-images/profile-img4.jpg",
      name: "Marija Nestorovic",
      position: "Sales force, ex-Microsoft",
    },
    {
      id: 3,
      profile:
        "https://images.unsplash.com/photo-1487412720507-e7ab37603c6f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80",
      name: "Smriti Sharma",
      position: "Freshdesk, Odesk",
    },
  ];

  return (
    <div className={styles.container}>
      {/* <Head>
        <title>Home</title>
        <link
          href="https://fonts.googleapis.com/css2?family=Poppins&display=swap"
          rel="stylesheet"
        />
      </Head> */}
      <div className={styles.header}>
        <div>
          <p>Product Management</p>
          <p>Evaluating Designs with users: Usability Testing done right</p>
        </div>
        <div>
          <Icon icon={share2} />
        </div>
      </div>
      <div>
        <p>7 Videos &emsp;|&emsp; 26 Min</p>
      </div>
      <div className={styles.listWrapper}>
        <div className={styles.list}>
          {usersData.map((user, id) => (
            <UserList
              key={id}
              profile={user.profile}
              name={user.name}
              position={user.position}
            />
          ))}
        </div>
        <div className={styles.totalVideos}>+12</div>
      </div>
    </div>
  );
}

export default PlayList;
