import styles from "./Home.module.scss";
import PlayList from "./PlayList";
import Icon from "react-icons-kit";
import { androidArrowDropright } from "react-icons-kit/ionicons/androidArrowDropright";
import { androidArrowDropleft } from "react-icons-kit/ionicons/androidArrowDropleft";

function Home() {
  return (
    <div className={styles.container}>
      <div className={styles.trendingPlaylistWrapper}>
        <div className={styles.header}>
          <div>
            <p>Trending Playlist</p>
            <p>Trending videos for you this week in Product Management</p>
          </div>
          <div className={styles.nextPrev}>
            <div>
              <Icon icon={androidArrowDropleft} />
            </div>
            <div>
              <Icon icon={androidArrowDropright} />
            </div>
          </div>
        </div>
        <div className={styles.playlist}>
          <PlayList />
          <PlayList />
          <PlayList />
          <PlayList />
        </div>
      </div>
    </div>
  );
}

export default Home;
