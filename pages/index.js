import Head from "next/head";
import Link from "next/link";

export default function Home() {
  return (
    <div>
      <Link href="/Home">
        <a>Home</a>
      </Link>
    </div>
  );
}
